var BBP = {};
BBP.calculator = {
  calc: $('.js-calc'),
  group: $('.js-group', this.calc),
  btn: $('.js-calc-btn', this.calc),
  descEl: $('.js-cal-desc', this.calc),
  resultEl: $('.js-calc-result', this.calc),
  chkTerm: $('.js-chk-term', this.calc),
  selectModelEl: $('#model'),
  resultTextEl: $('.js-result-txt', this.calc),
  errorEl: $('.bbp-calc__error', this.calc),
  data: [],
  modelName: '',
  modelItems: [],
  maxRafterLength: 50.4,

  init: function() {
    var self = this;
    self.group.hide();
    self.descEl.hide();
    self.resultEl.hide();

    self.addEvents();
    self.data = self.getDataFromJson();
  },

  addEvents: function() {
    var self = this;

    self.selectModelEl.on('change', function(){
      self.group.hide();
      self.descEl.hide();
      self.modelName = $(this).val();
      if (self.modelName != 'select') {
        self.resultEl.hide();
        self.descEl.show();
        $('.js-all').show();
        $('.js-'+$(this).val()).show();
        self.selectModelEl.next('.bbp-calc__error').hide();
        self.setTabIndex();
        self.getModelData();
      }
      $('.js-rafter-limit').html(self.maxRafterLength);
    });

    $('input').on('change', function() {
      self.showErrorMsg();
      self.changeDescription($(this));
    });

    self.btn.on('click', function(event) {
      event.preventDefault();

      if (self.showErrorMsg()) {
        var groupData = self.getGroupFromData();

        if (+$('#rafterL').val() <= self.maxRafterLength) {

          self.resultTextEl.hide();

          //draw image
          var img = $('.js-result-img');
          img.attr('src', 'images/' + groupData.image);
          img.attr('alt', self.modelName);

          self.resultEl.show();
        } else {
          self.resultEl.hide();
          self.resultTextEl.show();
        }
      }
    });

    self.limitDigitsAfterComma();
  },

  numberBetween: function(val, min, max) {
    return +val >= +min && +val <= +max;
  },

  showErrorMsg: function() {
    var self = this;
    var result = true;

    self.errorEl.hide();

    if ( !self.chkTerm.is(':checked') ) {
      self.chkTerm.next('.bbp-calc__error').show();
      result = false;
    }

    if ( self.selectModelEl.val() == 'select' ) {
      self.selectModelEl.next('.bbp-calc__error').show();
      result = false;
    }

    $('.js-calc input').each(function() {
      if( $(this).parent().css('display') != 'none' && $(this).val() == '' ) {
        $(this).parent().find('.bbp-calc__error').show();
        result = false;
      }
    });

    return result;
  },

  limitDigitsAfterComma: function () {
    var self = this;
    $('input[type=number]').on('keypress', function(e) {
        var character = String.fromCharCode(e.keyCode);
        var newValue = this.value + character;

        if (isNaN(newValue) || self.hasDecimalPlace(newValue, 2)) {
          e.preventDefault();
          return false;
        }
    });
  },

  hasDecimalPlace: function(value, x) {
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
  },

  getFormValue: function() {
    var values = {};
    var allFormValues = $('form').serializeArray();

    $.each(allFormValues, function(i, field) {
      if (field.value) {
        values[field.name] = field.value;
      }
    });

    return values;
  },

  getDataFromJson: function() {
    var self = this;
    $.getJSON('js/data.json', function(json) {
      self.data = json.modules.items;
    });
  },

  getModelData: function() {
    var self = this;
    self.modelItems = self.data.filter(function(item) { return item.name === self.modelName; });
    self.maxRafterLength = +self.modelItems[0].groups[self.modelItems[0].groups.length-1].max;
  },

  getGroupFromData: function() {
    var self = this;
    var group = {};
    var values = self.getFormValue();
    self.modelName = values.model;
    var rafterLength = values.rafterL;

    if ( self.modelItems.length ) {
      self.modelItems[0].groups.filter(function(item) {
        if ( self.numberBetween(rafterLength, item.min, item.max) ) {
          group.image =  item.image;
          group.group =  item.group;
        }
      });
    }
    return group;
  },

  setTabIndex: function() {
    $(':input').each(function (i) {
      if($(this).parent().css('display') != 'none' && $(this).val() == '') {
        $(this).attr('tabindex', i + 1);
      }
    });
  },

  changeDescription: function(input) {
    $('.js-panels').text($('#rafterL').val());

    if ( input.is('[id$="spacing"]') ) {
      var spacingText = input.val() + '" ' + input.parent().find('label').text().toLowerCase();
      $('.js-spacing').text( spacingText );
    }

    //$('.js-psf-snowload').text();
  }
};

$(function() {
  BBP.calculator.init();
});